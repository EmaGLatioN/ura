# ura



## Давайте начнем
 Приложение разработано для удобства читателей, своего рода библиотека. Реализован функционал 
получения, фмльтров, сортировки, добавления, изменения книг для авторов. Реализована проверка наличия прав доступа.
При старте работ не забудьте изменить DEBUG = True на False в настройках проекта.
При необходимости обернуть проект в Docker запустите Dockerfile:
  docker build -t la_docker.


Запуск сервера: ./manage.py runserver
## Примеры запросов
Для того что бы создать книгу, необходимо сначала добавить автора.

-Пример создания автора:
mutation createAuthor {  
  createActor(input: {
    firstName: "Tom"
    lastName: "Riddle"
  }) {
    ok
    author {
      id
      firstName
      lastName
    }
  }
}

- Создание/добавление книги:

mutation createBook {  
  createBook(input: {
    title: "Away",
    genres: "Anyway"
    authors: [
      {
        id: 1
      }
    ]
  }) {
    ok
    book{
      id
      title
      genres {
        id
      }
      authors {
        id
        firstName
        lastName
      }
      releaseDate
    }
  }
}
- Обновление/изменение книги:
mutation updateMovie {  
  updateBook(id: 2, input: {
    title: "Away1",
    genres: "Anyway1"
    authors: [
      {
        id: 3
      }
    ]

  }) {
    ok
    book{
      id
      title
      genres {
        id
      }
      authors {
        id
        firstName
        lastName
      }
    }
  }
}
Получение всех книг:  

query {
 books {
  id 
  title 
  authors {
    id
  } 
  genres {
    id
  } 
  releaseDate 
} 
}  
- Получение отсортированных книг по автору:  

query {
 books(sortBy: "authors") {
  id 
  title 
  authors {
    id
  } 
  genres {
    id
  } 
  releaseDate 
} 
}  
- Получение отфильтрованных книг по названию:  

query {
 books(filterBy: "Python") {
  id 
  title 
  authors {
    id
  } 
  genres {
    id
  } 
  releaseDate 
} 
}
- Получение книг интервалу дат:

query {
 books(startDate: "2022-01-01", endDate: "2022-12-31") {
  id 
  title 
  releaseDate 
  authors {
    id
  } 
  genres {
    id
  } 
} 
}

## Отключение GraphQL
- В параметре graphiql нужно указать False
    Для получения пост запросов.
urlpatterns = [  
    path('admin/', admin.site.urls),
    path('graphql/', csrf_exempt(GraphQLView.as_view(graphiql=False))),
]