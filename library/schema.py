from .models import Genre, Author, Book
import graphene
from graphene_django.types import DjangoObjectType, ObjectType
from django.contrib.auth.decorators import login_required, permission_required

"""
Создание схемы, мутаций, фильтров для получения книг,авторов.
"""


class GenreType(DjangoObjectType):
    class Meta:
        model = Genre


class AuthorType(DjangoObjectType):
    class Meta:
        model = Author


class BookType(DjangoObjectType):
    class Meta:
        model = Book


class Query(ObjectType):
    """
    Класс используется для получения сортировки и фильтрации данных.
    Методы, которые мы создали в классе Query, Resolvers. Resolvers связывает запросы в схеме с реальными действиями, выполняемыми базой данных.
    Мы взаимодействуем с нашей базой данных через модели.
     Мы получаем ID из параметров запроса и возвращаем актера из нашей базы данных с этим ID в качестве его первичного ключа.
    Функция resolve_actors просто получает всех актеров в базе данных и возвращает их в виде списка.
    Аналогично и с другими методами.
    """
    genre = graphene.Field(GenreType)
    author = graphene.Field(AuthorType)
    book = graphene.Field(BookType)
    genres = graphene.List(GenreType)
    authors = graphene.List(AuthorType)
    books = graphene.List(
        BookType,
        sort_by=graphene.String(),
        filter_by=graphene.String(),
        start_date=graphene.Date(),
        end_date=graphene.Date()
    )

    def resolve_genre(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            return Genre.objects.get(pk=id)
        return None

    def resolve_author(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            return Author.objects.get(pk=id)
        return None

    def resolve_book(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            return Book.objects.get(pk=id)
        return None

    def resolve_genres(self, info, **kwargs):
        return Genre.objects.all()

    def resolve_authors(self, info, **kwargs):
        return Author.objects.all()

    """
    Метод для реализации фильтров для книг и их получения 
    """
    def resolve_books(self, info, sort_by=None, filter_by=None, start_date=None, end_date=None, **kwargs):
        books = Book.objects.all()
        if sort_by == 'author':
            books = books.order_by('author')
        elif sort_by == 'genre':
            books = books.order_by('genre')
        elif sort_by == 'release_date':
            books = books.order_by('release_date')
        elif sort_by == 'id':
            books.order_by('id')

        if start_date and end_date:
            books = books.filter(release_date__range=[start_date, end_date])
        if filter_by:
            books = books.filter(title__icontains=filter_by)

        return books


class GenreInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()
    link = graphene.String()
    text = graphene.String()


class AuthorInput(graphene.InputObjectType):
    id = graphene.ID()
    first_name = graphene.String()
    last_name = graphene.String()


class BookInput(graphene.InputObjectType):
    title = graphene.String()
    authors = graphene.List(AuthorInput)
    release_date = graphene.Int()
    genres = graphene.List(GenreInput)


class CreateAuthor(graphene.Mutation):
    """
    Классы Create и Update определяют поля для изменения и обновления в API.
    Аналогично для классов книг. С поправкой на права доступа.
    """
    class Arguments:
        input = AuthorInput(required=True)
    ok = graphene.Boolean()
    author = graphene.Field(AuthorType)

    @staticmethod
    def mutate(root, info, input=None):
        ok = True
        author_instance = Author(
            first_name=input.first_name,
            last_name=input.last_name
        )
        author_instance.save()
        return CreateAuthor(ok=ok, author=author_instance)


class UpdateAuthor(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = AuthorInput(required=True)
    ok = graphene.Boolean()
    author = graphene.Field(AuthorType)

    @staticmethod
    def mutate(root, info, id, input=None):
        ok = False
        author_instance = Author.objects.get(pk=id)
        if author_instance:
            ok = True
            author_instance.name = input.name
            author_instance.save()
            return UpdateAuthor(ok=ok, author=author_instance)
        return UpdateAuthor(ok=ok, author=None)


class CreateBook(graphene.Mutation):
    class Arguments:
        input = BookInput(required=True)
    ok = graphene.Boolean()
    book = graphene.Field(BookType)

    @staticmethod
    def mutate(root, info, input=None):
        ok = True
        authors = []
        for author_input in input.authors:
            author = Book.objects.get(pk=author_input.id)
            if author is None:
                return CreateBook(ok=False, movie=None)
            authors.append(author)
        book_instance = Book(
            title=input.title,
            release_date=input.release_date,
            genres=input.genres
        )
        book_instance.save()
        book_instance.authors.set(authors)
        return CreateBook(ok=ok, book=book_instance)


class UpdateBook(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = BookInput(required=True)
    ok = graphene.Boolean()
    book = graphene.Field(BookType)

    @staticmethod
    def mutate(root, info, id, input=None):
        ok = False
        book_instance = Book.objects.get(pk=id)
        if book_instance:
            ok = True
            authors = []
            for author_input in input.authors:
                author = Author.objects.get(pk=author_input.id)
                if author is None:
                    return CreateBook(ok=False, book=None)
                authors.append(author)
            book_instance = Book(
                title=input.title,
                release_date=input.release_date,
                genres=input.genres
            )
            book_instance.save()
            book_instance.authors.set(authors)
            return UpdateBook(ok=ok, book=book_instance)
        return UpdateBook(ok=ok, book=None)


class Mutation(graphene.ObjectType):
    create_actor = CreateAuthor.Field()
    update_actor = UpdateAuthor.Field()
    create_book = CreateBook.Field()
    update_book = UpdateBook.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)