from django.db import models
from django.core.validators import (FileExtensionValidator)

"""
Модели с зависимостями для жанра, автора, книги
"""


class Genre(models.Model):
    class Meta:
        verbose_name = 'Жанр'
    name = models.CharField(max_length=255)
    link = models.URLField()
    text = models.TextField()

    def __str__(self):
        return self.name


class Author(models.Model):
    class Meta:
        verbose_name = 'Автор'
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.first_name}{self.last_name}'


class Book(models.Model):
    class Meta:
        verbose_name = 'Книга'
    title = models.CharField(max_length=255)
    authors = models.ManyToManyField(Author)
    release_date = models.DateField()
    genres = models.ManyToManyField(Genre)
    cover = models.FileField(null=True, blank=True, validators=[FileExtensionValidator(
        allowed_extensions=['xlsx', 'jpg', 'png'],
        message='Не тот файл!'
    )])
