from .models import Genre, Author, Book
from django.forms import ModelForm


class GenreForm(ModelForm):
    class Meta:
        model = Genre
        fields = ['name', 'link', 'text']


class AuthorForm(ModelForm):
    class Meta:
        model = Author
        fields = ['first_name', 'last_name']


class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = ['title', 'authors', 'release_date', 'genres', 'cover']