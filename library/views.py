from django.shortcuts import render
from django.http import JsonResponse
from ariadne_django.views import GraphQLView


class CustomGraphQLView(GraphQLView):
    def get_context(self, request):
        context = super().get_context(request)
        return context


graphql_view = CustomGraphQLView.as_view


