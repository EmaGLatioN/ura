# Generated by Django 4.1.6 on 2023-10-30 19:57

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0002_feedfile'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='cover',
            field=models.FileField(blank=True, null=True, upload_to='', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['xlsx', 'jpg', 'png'], message='Не тот файл!')]),
        ),
        migrations.DeleteModel(
            name='FeedFile',
        ),
    ]
