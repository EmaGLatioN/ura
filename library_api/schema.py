import graphene
import library_api.library.schema


class Query(library_api.library.schema.Query, graphene.ObjectType):

    pass


class Mutation(library_api.library.schema.Mutation, graphene.ObjectType):

    pass


schema = graphene.Schema(query=Query, mutation=Mutation)