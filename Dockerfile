FROM python:3.11

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFRED 1

RUN pip install --upgrade pip

RUN apt-get update && apt-get upgrade -qy
RUN useradd -rms /bin/bash la && chmod 777 /opt /run

WORKDIR /la

RUN mkdir /la/static && mkdir /la/media && chown -R la:la /la && chmod 755 /la

COPY --chown=la:la . .

RUN pip install -r requirements.txt

USER la

CMD [ "./manage.py runserver","-b", "soaqaz.wsgi:application" ]
